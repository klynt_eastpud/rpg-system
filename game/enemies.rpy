
init python:

    class LeonMaksutov(EnemyCharacter):
        base_stats = BaseStats(vitality=15, defence=3, strength=5, speed=2)
        EXP_MULTIPLIER = 5
        name = 'Scarlet-clad pirate'
        skills = [(Attack, 100)]
        sprite = "images/sprites/pirates/max.png"
        active_sprite = im.MatrixColor("images/sprites/pirates/max.png", im.matrix.brightness(0.2))


    class VelaOrganon(EnemyCharacter):
        base_stats = BaseStats(vitality=4, defence=4, strength=8, speed=2)
        EXP_MULTIPLIER = 5
        name = 'Baton-wielding pirate'
        skills = [(Attack, 100)]
        sprite = "images/sprites/pirates/vela.png"
        active_sprite = im.MatrixColor("images/sprites/pirates/vela.png", im.matrix.brightness(0.2))


    class AndroidModelXephyr(EnemyCharacter):
        base_stats = BaseStats(vitality=9, defence=8, strength=7, speed=1)
        EXP_MULTIPLIER = 5
        name = 'Visor-wearing pirate'
        skills = [(Attack, 100)]
        sprite = "images/sprites/pirates/xephyr.png"
        active_sprite = im.MatrixColor("images/sprites/pirates/xephyr.png", im.matrix.brightness(0.2))


    class TimeEntity(EnemyCharacter):
        base_stats = BaseStats(vitality=60, defence=2, strength=3, speed=3)
        EXP_MULTIPLIER = 5
        name = "???"
        skills = [(Attack, 80), (Gravity, 15), (Ultimatum, 5)]
        sprite = "images/time.png"
        active_sprite = im.MatrixColor("images/time.png", im.matrix.brightness(0.2))
