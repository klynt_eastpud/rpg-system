
# Inventory

screen inventory_option(options):
    modal True

    zorder 2

    python:
        x, y = renpy.get_mouse_pos()
        xval = 1.0 if x > config.screen_width/2 else .0
        yval = 1.0 if y > config.screen_height/2 else .0

    frame:
        pos (x, y)
        anchor (xval, yval)
        xsize 300
        padding (20,20)

        vbox:
            xalign 0.5
            yalign 0.5
            spacing 10
            xsize target_window_width

            for (o_caption, o_action) in options:
                if o_action is None:
                    text o_caption xfill True bold True
                else:
                    textbutton o_caption action o_action xfill True hover_background gui.hover_color text_hover_color gui.choice_button_text_hover_color


style increased_colour:
    color "#4af"


style reduced_colour:
    color "#f44"

style stat_frame:
    background None
    padding (0,0)
    ysize 30

style heading_xlarge:
    size 40
    bold True

style heading_large:
    size 30

style heading_medium:
    size 26


define stat_value_width = 140
define stat_name_width = 50
define party_member_spacing = 50


screen inventory():
    tag menu

    $ tooltip = GetTooltip()

    frame:
        xsize 1280
        ysize 720
        padding (20, 20)

        hbox:
            # inventory
            vbox:
                spacing 10

                text 'Items' style "heading_xlarge"

                # CONSUMABLES

                for (set_title, item_list) in inventory.consumable_items_display:

                    text '[set_title]' style "heading_large"

                    vbox:
                        for item in item_list:
                            python:
                                options = [
                                    (member.name, [Function(member.use_item, item), Hide('inventory_option')])
                                    for member in player_party.members
                                ] + [('Cancel', Hide('inventory_option'))]
                                use_options = [
                                    ('Use', [Show('inventory_option', options=options)]),
                                    ('Cancel', Hide('inventory_option')),
                                ]

                            textbutton '[item.name]' style "option_button":
                                tooltip item.tooltip
                                action ShowMenu('inventory_option', use_options)
                                text_hover_color gui.choice_button_text_hover_color
                                hover_background gui.hover_color
                                xsize 300

                text 'Equipment' style "heading_large"

                for (set_title, item_list) in inventory.equippable_items_display:

                    vbox:
                        text '[set_title]' style "heading_medium"

                        for item in item_list:
                            python:
                                options = [
                                    (member.name, [Function(member.equip_item, item), Hide('inventory_option')])
                                    for member in player_party.members
                                ] + [('Cancel', Hide('inventory_option'))]
                                equip_options =  [
                                    ('Equip', [Show('inventory_option', options=options)]),
                                    ('Cancel', Hide('inventory_option')),
                                ]
                            textbutton '[item.name]' style "option_button":
                                tooltip item.equip_tooltip
                                action ShowMenu('inventory_option', equip_options)
                                text_hover_color gui.choice_button_text_hover_color
                                hover_background gui.hover_color
                                xsize 300

                        if not item_list:
                            text 'Empty' color gui.idle_color

            # party
            vbox:
                spacing 10

                text 'Party' style "heading_xlarge"

                hbox:
                    spacing party_member_spacing

                    for member in player_party.members:

                        vbox:

                            text '[member.name]' style "heading_large"

                            frame:
                                background None
                                margin (-5,10,0,0)

                                text 'Level [member.level]'

                            hbox:
                                frame style "stat_frame":
                                    xsize stat_name_width

                                    text 'HP: '

                                frame style "stat_frame":
                                    xsize stat_value_width

                                    hbox:
                                        xalign 1.0
                                        text '[member.current_hp]/'

                                        if tooltip and isinstance(tooltip, dict) and tooltip['equippable'] and 'max_hp' in tooltip['affected_stats'] and (tooltip['target'] == member or tooltip['target'] is None):

                                            $ modified_max_hp = member.max_hp + tooltip['effect']['max_hp']

                                            if modified_max_hp > member.max_hp:
                                                text '[modified_max_hp]' style "increased_colour"

                                            elif modified_max_hp == member.max_hp:
                                                text '[modified_max_hp]'

                                            else:
                                                text '[modified_max_hp]' style "reduced_colour"

                                        else:
                                            text '[member.max_hp]'

                            for (stat_title, stat_name, stat_value) in member.all_stats:
                                hbox:

                                    frame style "stat_frame":
                                        xsize stat_name_width

                                        text '[stat_title]:'

                                    $ modified_stat = None

                                    frame style "stat_frame":
                                        xsize stat_value_width

                                        if tooltip and isinstance(tooltip, dict) and tooltip['equippable'] and stat_name in tooltip['affected_stats'] and (tooltip['target'] == member or tooltip['target'] is None):

                                            $ modified_stat = stat_value + tooltip['effect'][stat_name]

                                            if modified_stat > stat_value:
                                                text '[modified_stat]' style "increased_colour" xalign 1.0

                                            elif modified_stat == stat_value:
                                                text '[modified_stat]' xalign 1.0

                                            else:
                                                text '[modified_stat]' style "reduced_colour" xalign 1.0

                                        else:
                                            text '[stat_value]' xalign 1.0


                            for (slot_name, equipped_item) in member.all_equipment_display:
                                python:
                                    unequip_options = [
                                        ('Remove', [Function(member.unequip_item, equipped_item), Hide('inventory_option')]),
                                        ('Cancel', Hide('inventory_option')),
                                    ]

                                vbox:
                                    ypos 20

                                    text '[slot_name]:'

                                    frame:
                                        background None
                                        margin (10,0,0,0)

                                        if equipped_item:
                                            textbutton '[equipped_item.name]' style "option_button":
                                                tooltip equipped_item.unequip_tooltip
                                                action ShowMenu('inventory_option', unequip_options)
                                                text_hover_color gui.choice_button_text_hover_color
                                                hover_background gui.hover_color
                                        else:
                                            text 'Empty' color gui.idle_color

        if tooltip and isinstance(tooltip, dict):
            python:
                x, y = renpy.get_mouse_pos()
                xval = 1.0 if x > config.screen_width/2 else .0
                yval = 1.0 if y > config.screen_height/2 else .0

            frame:
                pos (x, y)
                anchor (xval, yval)
                xsize 300
                padding (20,20)

                vbox:
                    for line in tooltip['description']:
                        text "[line]"


    textbutton _("Return") style "return_button" action Return()

# Battle screens

define target_window_width = 250
define target_window_x = 60
define hp_min_width = 150
define player_box_width = 300
define player_atb_width = MAX_PRIORITY / 6
define player_box_height = 40
define player_party_display_y = 720 - target_window_x
define player_party_display_x = 1280 - 20
define fill_colour = gui.muted_color
define full_colour = gui.hover_muted_color
define bg_colour = gui.interface_text_color
define active_outlines = [(2, "#fff")]
define active_colour = "#000"
define player_damage_ypos = 720 - player_box_height - 150


transform player_left:
    xpos player_party_display_x - (player_box_width * 2) + 10
    ypos player_party_display_y - 32
    yanchor 1.0
    xanchor 1.0

transform player_right:
    xpos player_party_display_x - (player_box_width * 2) + 10
    ypos player_party_display_y + 10
    yanchor 1.0
    xanchor 1.0


screen outcome_display(outcome_text, damage_position, character_type):

    frame at damage_position:
        xsize 400
        background None

        add outcome_text at bounce:
            if character_type == 'enemy':
                xalign 0.5
            else:
                xalign 1.0

    timer 0.6 action Hide('outcome_display')


screen battle_choice(items):

    frame:
        xpos target_window_x
        ypos player_party_display_y
        yanchor 1.0
        padding (20, 20)

        vbox:
            xalign 0.5
            yalign 0.5
            spacing 10
            xsize target_window_width

            for i in items:
                if i.action is None:
                    text i.caption xfill True bold True
                else:
                    textbutton i.caption action i.action xfill True hover_background gui.hover_color text_hover_color gui.choice_button_text_hover_color


## When this is true, menu captions will be spoken by the narrator. When false,
## menu captions will be displayed as empty buttons.
define config.narrator_menu = True


screen battle(battle):
    zorder 0

    # enemy party display
    window:
        background None
        padding (0,0)
        margin (-5,-5)
        xsize 1280
        ysize 720

        for fighter in battle.enemy_party.members:

            $ name = fighter.name
            $ current_hp = fighter.current_hp
            $ max_hp = fighter.max_hp
            $ sprite = fighter.sprite
            $ active_sprite = fighter.active_sprite
            $ sprite_pos = fighter.position

            if not fighter.ko:
                if sprite:
                    frame at sprite_pos:
                        background None
                        xalign 0.5
                        add sprite

                if debug_mode:
                    frame at sprite_pos:
                        xsize 400
                        text "[current_hp]/[max_hp]" style "battle_text":
                            xalign 0.5 yalign 0.5

            if fighter.active:
                frame at sprite_pos:
                    background None
                    xalign 0.5
                    add active_sprite at flash_in_out

    # debug
    if debug_mode:
        frame:
            background None

            vbox:
                for fighter in battle.player_party.members:
                    text '[fighter.name]' style "battle_text"
                    text 'STR: [fighter.strength]' style "battle_text"
                    text 'DEF: [fighter.defence]' style "battle_text"
                    text 'SPD: [fighter.speed]' style "battle_text"

                    hbox:
                        text 'STATUS:' style "battle_text"
                        for status in fighter.all_battle_statuses:
                            text '[status.affected_stat] [status.amount]' style "battle_text"

    # party display
    frame:
        background None
        ypos player_party_display_y
        yanchor 1.0
        xanchor 1.0
        xpos player_party_display_x

        vbox:
            spacing 5

            for fighter in battle.player_party.members:
                $ name = fighter.name
                $ current_hp = fighter.current_hp
                $ max_hp = fighter.max_hp

                hbox:
                    xsize player_box_width
                    ysize player_box_height

                    hbox:
                        spacing 10
                        xsize player_atb_width

                        # Name display
                        frame:
                            xsize player_atb_width
                            background None

                            # hacky fix to stop active text shunting the box to the left for some reason ??
                            text '' style "battle_text"

                            text '[name]' style "battle_text":
                                xfill True
                                xalign 1.0

                            if fighter.active:
                                text '[name]' style "battle_text" at flashing:
                                    outlines active_outlines
                                    color active_colour
                                    xfill True
                                    xalign 1.0

                        # HP display
                        frame:
                            background None
                            xsize hp_min_width

                            text '' style "battle_text"

                            text '[current_hp]/[max_hp]':
                                if current_hp <= max_hp * 0.5 and current_hp > max_hp * 0.2:
                                    style "battle_warning"
                                elif current_hp <= max_hp * 0.2:
                                    style "battle_danger"
                                else:
                                    style "battle_text"

                            if fighter.active:
                                text '[current_hp]/[max_hp]' at flashing:
                                    outlines active_outlines
                                    color active_colour

                                    if current_hp <= max_hp * 0.5 and current_hp > max_hp * 0.2:
                                        style "battle_warning"
                                    elif current_hp <= max_hp * 0.2:
                                        style "battle_danger"
                                    else:
                                        style "battle_text"


                            # ATB gauges
                        frame:
                            background None
                            padding (0,0)
                            ysize 15
                            ypos 15
                            xsize player_atb_width + 4

                            add Solid(bg_colour, xsize=player_atb_width + 4)

                            if fighter.active:
                                add Solid('#fff', xsize=player_atb_width + 4) at flashing

                            frame:
                                background None
                                padding (2,2)
                                ysize 15
                                xsize player_atb_width

                                if fighter.priority < MAX_PRIORITY:
                                    add Solid(fill_colour, xsize=(fighter.priority)/6)

                                elif fighter.active:
                                    add Solid(bg_colour, xsize=player_atb_width)
                                    add Solid(full_colour, xsize=player_atb_width)

                        # Status display
                        frame:
                            background None
                            xsize player_box_width - player_atb_width

                            text '' style "battle_text"

                            hbox:
                                for status in fighter.all_battle_statuses:
                                    add status.icon zoom 0.5

style battle_text:
    color "#fff"
    outlines [(2, "#333")]
    size 30

style battle_warning is battle_text:
    color "#fe0"

style battle_danger is battle_text:
    color "#f00"
