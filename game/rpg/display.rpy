init offset = -3

init:

    transform bounce:
        parallel:
            alpha 0.0
            ease 0.1 alpha 1.0
            pause 0.4
            ease 0.1 alpha 0.0
        parallel:
            yoffset 50
            ease 0.1 yoffset -10
            ease 0.1 yoffset 0

    transform flash_in_out:
        alpha 1.0
        ease 0.1 alpha 0.0
        ease 0.1 alpha 1.0

    transform flashing:
        alpha 1.0
        ease 0.4 alpha 0.0
        ease 0.4 alpha 1.0
        repeat

    # Positions
    $ enemy_yalign = 1.0
    $ damage_yalign = 0.4
    $ player_yalign = 0.7

    transform damage_center:
        xalign 0.5
        yalign damage_yalign

    transform damage_left:
        xalign 0.25
        yalign damage_yalign

    transform damage_right:
        xalign 0.75
        yalign damage_yalign

    transform damage_far_left:
        xalign 0.1
        yalign damage_yalign

    transform damage_far_right:
        xalign 0.9
        yalign damage_yalign

    transform enemy_center:
        xalign 0.5
        yalign enemy_yalign

    transform enemy_left:
        xalign 0.25
        yalign enemy_yalign

    transform enemy_right:
        xalign 0.75
        yalign enemy_yalign

    transform enemy_far_left:
        xalign 0.1
        yalign enemy_yalign

    transform enemy_far_right:
        xalign 0.9
        yalign enemy_yalign

    ## EFFECTS
    $ flashwhite = Fade(0.1, 0.0, 0.4, color='#fff')
    $ flashgreen = Fade(0.1, 0.0, 0.4, color='#6fa')
    $ flashred = Fade(0.1, 0.0, 0.4, color='#ff0000')
    $ flashblack = Fade(0.1, 0.0, 0.4, color='#000')
