init offset = -3
init python:
    import math


    class BaseStats(object):
        """
        This inner class probably won't change after init.
        Modifiers will instead be applied to the derived stats in BasicCharacter.
        """
        def __init__(self, vitality, defence, strength, speed):
            self.vitality = vitality
            self.defence = defence
            self.strength = strength
            self.speed = speed


    class BasicCharacter(object):

        def __init__(self, level=1, *args, **kwargs):
            self.priority = 0
            self.active = False
            self.stat_modifiers = {
                'battle': {
                    'max_hp': [],
                    'strength': [],
                    'defence': [],
                    'speed': [],
                },
                'equipment': {
                    'max_hp': [],
                    'strength': [],
                    'defence': [],
                    'speed': [],
                }
            }
            self.equipped_items = {
                'weapon': None,
                'armour': None,
                'accessory': None
            }
            self.level = level

        @property
        def all_equipment_display(self):
            return [
                ('Weapon', self.equipped_items['weapon']),
                ('Armour', self.equipped_items['armour']),
                ('Accessory', self.equipped_items['accessory']),
            ]

        @property
        def equipment(self):
            return self.equipped_items['weapon'] + self.equipped_items['armour'] + self.equipped_items['accessory']

        @property
        def all_battle_statuses(self):
            statuses = []
            for (stat, modifiers) in self.stat_modifiers['battle'].iteritems():
                statuses += modifiers
            return sorted(statuses, key=lambda status: status.duration)

        @property
        def all_stats(self):
            return [
                ('Speed', 'speed', self.speed),
                ('Strength', 'strength', self.strength),
                ('Defence', 'defence', self.defence),
            ]

        @property
        def ko(self):
            return self.current_hp == 0

        @property
        def speed(self):
            speed = self.get_derived_stat(stat_name='speed', stat_multiplier=SPEED_MULTIPLIER, stat_exponent_multiplier=SPEED_EXPONENT_MULTIPLIER)

            # apply modifiers
            speed += self.sum_modifiers_for_stat(stat_name='speed', stat_type='equipment')
            speed += self.sum_modifiers_for_stat(stat_name='speed', stat_type='battle') * speed / 100

            if speed <= 0:
                return 1

            return int(math.ceil(speed))

        @property
        def max_hp(self):
            max_hp = self.get_derived_stat(stat_name='vitality', stat_multiplier=VIT_MULTIPLIER)

            # apply modifiers
            max_hp += self.sum_modifiers_for_stat(stat_name='max_hp', stat_type='equipment')
            max_hp += self.sum_modifiers_for_stat(stat_name='max_hp', stat_type='battle') * max_hp / 100

            return int(math.ceil(max_hp))

        @property
        def defence(self):
            defence = self.get_derived_stat(stat_name='defence')

            # apply modifiers
            defence += self.sum_modifiers_for_stat(stat_name='defence', stat_type='equipment')
            defence += self.sum_modifiers_for_stat(stat_name='defence', stat_type='battle') * defence / 100

            if defence <= 0:
                return 1

            return int(math.ceil(defence))

        @property
        def strength(self):
            strength = self.get_derived_stat(stat_name='strength', stat_multiplier=STR_MULTIPLIER)

            # apply modifiers
            strength += self.sum_modifiers_for_stat(stat_name='strength', stat_type='equipment')
            strength += self.sum_modifiers_for_stat(stat_name='strength', stat_type='battle') * strength / 100

            if strength <= 0:
                return 1

            return int(math.ceil(strength))

        def get_derived_stat(self, stat_name, stat_multiplier=STAT_MULTIPLIER, stat_exponent_multiplier=1):

            base_stat = getattr(self.base_stats, stat_name)

            return (
                base_stat
                * pow(self.level, LEVEL_EXPONENT * stat_exponent_multiplier)
                + STAT_CONSTANTS_MAPPING[stat_name]
                * stat_multiplier
            )

        def equip_item(self, item):
            equipped_item = self.equipped_items[item.item_type]

            if equipped_item:
                inventory.add_items([equipped_item])

            item.target = self
            self.equipped_items[item.item_type] = item
            inventory.remove_item(item)

            # apply stat modifiers
            if item.stat_modifiers:
                for modifier in item.stat_modifiers:
                    self.stat_modifiers['equipment'][modifier.affected_stat].append(modifier)

                    # recalculate current_hp
                    if self.current_hp != self.max_hp:
                        self.current_hp += modifier.amount

        def unequip_item(self, item):
            item.target = None
            self.equipped_items[item.item_type] = None
            inventory.add_items([item])

            # unapply stat modifiers
            if item.stat_modifiers:
                for modifier in item.stat_modifiers:
                    self.stat_modifiers['equipment'][modifier.affected_stat].remove(modifier)

                    # recalculate current_hp
                    if self.current_hp != self.max_hp:
                        self.current_hp -= modifier.amount

        def use_item(self, item):
            # currently only HP restoration
            self.current_hp = max(0, min(self.current_hp + item.amount, self.max_hp))
            inventory.remove_item(item)

        def restore_hp_to_full(self):
            self.current_hp = self.max_hp

        def resolve_effect(self, effect):
            """
            Resolve damage and/or status effects.
            """
            if isinstance(effect, StatModifierStatus):

                effect_type = 'status'

                existing_modifiers = self.stat_modifiers['battle'][effect.affected_stat]
                # if the effect is not stackable and it already exists
                if not effect.stackable and any(status.name == effect.name for status in existing_modifiers):
                    return ('No effect!', effect_type)

                # add the status to stat modifiers
                self.stat_modifiers['battle'][effect.affected_stat].append(effect)
                # display the effect
                return (effect.effect_description, effect_type)

            elif isinstance(effect, Healing):

                effect_type = 'healing'

                max_healing = self.max_hp - self.current_hp
                self.current_hp += max(0, min(effect.amount, max_healing))

                return (effect.amount, effect_type)

            else:  # damage
                effect_type = 'damage'
                # cap at current_hp so we don't go below 0
                self.current_hp -= max(0, min(effect.amount, self.current_hp))
                return (effect.amount, effect_type)

        def update_status_effects(self):
            for (stat, modifiers) in self.stat_modifiers['battle'].iteritems():
                for i, modifier in enumerate(modifiers):
                    if modifier.duration == 0:
                        modifiers.pop(i)
                    else:
                        modifier.duration -= 1

        def tick(self):
            if not self.ko:
                self.priority += (self.speed * BATTLE_SPEED)

        def use_skill(self, skill, target):
            return skill.use(self, target)

        def get_action(self, enemies, players):
            skill = self.get_skill()
            target = self.get_target(enemies=enemies, players=players, skill=skill)
            return (skill, target)

        def sum_modifiers_for_stat(self, stat_name, stat_type):
            return sum([modifier.amount for modifier in self.stat_modifiers[stat_type][stat_name]])

        def knock_out(self):
            # remove all temporary stat modifiers on KO
            for (stat, modifiers) in self.stat_modifiers['battle'].iteritems():
                for i, modifier in enumerate(modifiers):
                    modifiers.pop(i)
