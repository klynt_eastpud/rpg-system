init -3 python:


    class PlayerCharacter(BasicCharacter):
        character_type = 'player'

        def __init__(self, name, base_stats, current_hp=None, attack=True, registered_skills=[], *args, **kwargs):
            super(PlayerCharacter, self).__init__(*args, **kwargs)
            self.base_stats = base_stats
            if current_hp:
                # mainly for debugging
                self.current_hp = current_hp
            else:
                self.current_hp = self.max_hp
            self.name = name
            self.registered_skills = registered_skills
            self.pending_exp = 0
            self.total_exp = 0
            self.attack = attack

        @property
        def skill_menu(self):
            skill_menu = {
                'main': [
                    (self.name, None),
                ]
            }

            if self.attack:
                skill_menu['main'].append(('Attack', Attack))

            if self.skills_submenu:
                skill_menu['skills'] = [('Skills', None)] + self.skills_submenu + [('Cancel', 'main')]
                skill_menu['main'].append(('Skills', 'skills'))

            return skill_menu

        @property
        def skills_submenu(self):
            return [(skill.name, skill) for skill in self.registered_skills]

        @property
        def exp_to_level(self):
            EXPONENT = 3
            BASE_EXP = 1000  # how much first level needs
            return int(math.floor(BASE_EXP * pow(self.level, EXPONENT)))

        @property
        def ready_to_level_up(self):
            return self.pending_exp >= self.exp_to_level

        def register_skill(self, skill):
            self.registered_skills.append(skill)

        def get_target(self, enemies, players, skill):
            name_label = [('Target', None)]
            cancel = [('Cancel', False)]
            if skill.skill_type == 'damage':
                targets = [(fighter.name, fighter) for fighter in enemies]
            elif skill.skill_type == 'healing':
                targets = [(fighter.name, fighter) for fighter in players if not fighter.ko]
            else:  # status
                targets = [(fighter.name, fighter) for fighter in players if not fighter.ko]
            choices = name_label + targets + cancel
            return renpy.display_menu(choices, interact=True, screen="battle_choice")

        def get_skill(self):
            chosen_option = 'main'

            while not hasattr(chosen_option, 'name'):
                chosen_option = renpy.display_menu(self.skill_menu[chosen_option], interact=True, screen="battle_choice")

            return chosen_option

        def add_exp(self, gained_exp):
            # add all to total and pending
            self.pending_exp += gained_exp
            self.total_exp += gained_exp

        def apply_pending_exp(self):
            # apply levels
            while self.pending_exp >= self.exp_to_level:
                # take exp_to_level from pending_exp
                self.pending_exp -= self.exp_to_level
                # then apply level up
                self.level += 1
                # repeat
