
init offset = -4
init python:

    # Characters
    STR_CONST = 1
    STR_MULTIPLIER = 24

    SPEED_MULTIPLIER = 0.5
    SPEED_CONST = 0
    SPEED_EXPONENT_MULTIPLIER = 0.1

    VIT_CONST = 1
    BASE_HP = 1
    VIT_MULTIPLIER = 127

    DEF_CONST = 1

    HEAL_CONST = 2

    STAT_MULTIPLIER = 0.6

    LEVEL_EXPONENT = 1.6
    LEVEL_MULTIPLIER = 1

    # Battle
    MAX_PRIORITY = 180 * 6
    BATTLE_SPEED = 3

    STAT_NAME_MAPPING = {
        'max_hp': 'Max HP',
        'current_hp': 'HP',
        'strength': 'Strength',
        'defence': 'Defence',
        'speed': 'Speed',
    }

    STAT_CONSTANTS_MAPPING = {
        'vitality': VIT_CONST,
        'strength': STR_CONST,
        'defence': DEF_CONST,
        'speed': SPEED_CONST,
    }
