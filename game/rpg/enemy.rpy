init offset = -2
init python:
    import random

    weighted_choice = lambda items : random.choice(sum(([val] * weight for val, weight in items), []))

    class EnemyCharacter(BasicCharacter):
        character_type = 'enemy'
        EXP_MULTIPLIER = 1

        def __init__(self, level=1, *args, **kwargs):
            super(EnemyCharacter, self).__init__()
            self.level = level
            self.current_hp = self.max_hp

        @property
        def exp_reward(self):
            return self.EXP_MULTIPLIER * self.level * 100

        def get_target(self, enemies, players, skill):
            active_players = [player for player in players if not player.ko]
            return random.choice(active_players)

        def get_skill(self):
            return weighted_choice(self.skills)
