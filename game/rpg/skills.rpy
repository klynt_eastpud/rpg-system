init offset = -3

init python:
    import random
    import math
    import uuid


    class Damage(object):
        def __init__(self, amount=0, element=None, crit=False):
            self.amount = amount
            self.element = element
            self.crit = crit


    class Healing(object):
        def __init__(self, amount=0, crit=False):
            self.amount = amount
            self.crit = crit


    class StatModifierStatus(object):
        def __init__(self, crit=False, amount=0, effect_description='', name='', affected_stat=None, duration=0, stackable=False, icon=''):
            self.name = name
            self.stackable = stackable
            self.effect_description = effect_description
            self.affected_stat = affected_stat
            # percentage increase/decrease
            self.amount = amount
            self.crit = crit
            # duration in turns
            self.duration = duration
            self.icon = icon
            # generate unique ID for this status effect
            self.uuid = uuid.uuid1()


    class DamageDealingSkill(object):
        pass


    class DamageRestoringSkill(object):
        pass


    class Attack(DamageDealingSkill):
        """
        Inflict damage based on strength.
        """
        name = 'Attack'
        special = False
        element = None
        effect = vpunch
        skill_type = 'damage'

        @classmethod
        def use(cls, user, target):
            DEF_MULTIPLIER = 2
            STR_MULTIPLIER = 10

            crit = random.randint(0, 20) == 20

            # base_dmg = user.strength * STR_MULTIPLIER
            base_dmg = user.strength

            # dmg_reduction = target.defence * DEF_MULTIPLIER / 100
            dmg_reduction = target.defence

            # +/- 10%
            dmg_variance = int(math.ceil(base_dmg * 0.1))

            total = base_dmg + random.randint(-dmg_variance, dmg_variance)

            # effective_dmg = total - (total * dmg_reduction)
            effective_dmg = total - dmg_reduction

            # double damage if crit
            if crit:
                amount = int(math.ceil(total * 2))
            else:
                amount = int(math.ceil(total))

            return Damage(
                amount=amount,
                element=cls.element,
                crit=crit,
            )


    class Gravity(DamageDealingSkill):
        """
        Inflict damage based on target's current hp.
        """
        name = 'Gravity'
        special = True
        element = None
        effect = flashblack
        skill_type = 'damage'

        @classmethod
        def use(cls, user, target):

            base_dmg = target.current_hp * 0.4

            # +/- 10%
            dmg_variance = int(math.ceil(base_dmg * 0.1))

            total = base_dmg + random.randint(-dmg_variance, dmg_variance)

            amount = int(math.ceil(total))

            return Damage(
                amount=amount,
                element=cls.element,
                crit=False,
            )


    class Ultimatum(DamageDealingSkill):
        """
        Reduce target's current hp to 1.
        """
        name = 'Ultimatum'
        special = True
        element = None
        effect = flashred
        skill_type = 'damage'

        @classmethod
        def use(cls, user, target):

            amount = target.current_hp - 1

            return Damage(
                amount=amount,
                element=cls.element,
                crit=False,
            )


    class Heal(DamageRestoringSkill):
        """
        Restore HP.
        """
        name = 'Heal'
        special = False
        element = None
        effect = flashgreen
        skill_type = 'healing'

        @classmethod
        def use(cls, user, target):

            crit = random.randint(0, 20) == 20

            heal = user.strength * HEAL_CONST

            # +/- 10%
            heal_variance = int(math.ceil(heal * 0.1))

            total = heal + random.randint(-heal_variance, heal_variance)

            # double healing if crit
            if crit:
                amount = int(math.ceil(total * 2))
            else:
                amount = int(math.ceil(total))

            return Healing(
                amount=amount,
                crit=crit,
            )


    class Repair(Heal):
        """
        Restore HP.
        """
        name = 'Repair'


    class BaseStatusSkill(object):
        skill_type = 'status'

        @classmethod
        def use(cls, user, target):
            return StatModifierStatus(
                name=cls.name,
                affected_stat=cls.affected_stat,
                amount=cls.amount,
                duration=cls.duration,
                effect_description=cls.effect_description,
                stackable=cls.stackable,
                crit=cls.crit,
                icon=cls.icon,
            )


    class Overclock(BaseStatusSkill):
        """
        Increases target's speed temporarily.
        """
        name = 'Overclock'
        effect = flashwhite
        special = True
        effect_description = 'Speed up!'
        stackable = False
        crit = False
        duration = 5
        affected_stat = 'speed'
        amount = 50 # +50%
        icon = "rpg/icons/overclock.png"


    class Fortitude(BaseStatusSkill):
        """
        Increases target's defence temporarily.
        """
        name = 'Fortitude'
        effect = flashwhite
        special = True
        effect_description = 'Defence up!'
        stackable = False
        crit = False
        duration = 5
        amount = 50  # +50%
        affected_stat = 'defence'
        icon = "rpg/icons/fortitude.png"


    class Courage(BaseStatusSkill):
        """
        Increases target's strength temporarily.
        """
        name = 'Courage'
        effect = flashwhite
        special = True
        effect_description = 'Strength up!'
        stackable = False
        crit = False
        duration = 5
        amount = 50  # +50%
        affected_stat = 'strength'
        icon = "rpg/icons/courage.png"
