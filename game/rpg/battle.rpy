init offset = -3
init python:
    import random

    random.seed(a=None)

    announcer = Character("battle_announcer")


    class Event(object):
        def __init__(self, name, data):
            self.name = name
            self.data = data


    class BattleEvent(object):
        def __init__(self, event_text, event_triggers):
            self.event_text = event_text
            self.event_triggers = event_triggers


    class Party(object):
        def __init__(self, character_type, members=[]):
            self.character_type = character_type
            self.members = members


    class BattleInstance(object):

        def __init__(self, player_party=None, enemy_party=None, battle_events=[], enemy_defeated_message="Enemy party defeated!", player_defeated_message="You died."):
            self.enemy_defeated_message = enemy_defeated_message
            self.player_defeated_message = player_defeated_message
            self.battle_events = battle_events
            self.player_party = player_party
            self.enemy_party = enemy_party
            self.success = False
            self.fighters = (self.player_party.members + self.enemy_party.members)

            if len(self.player_party.members) == 1:
                self.player_party.members[0].damage_position = player_right

            if len(self.player_party.members) == 2:
                self.player_party.members[0].damage_position = player_left
                self.player_party.members[1].damage_position = player_right

            # assign sprite positions
            # max 3 members in enemy party
            if len(self.enemy_party.members) == 1:
                self.enemy_party.members[0].position = enemy_center
                self.enemy_party.members[0].damage_position = damage_center

            if len(self.enemy_party.members) == 2:
                self.enemy_party.members[0].position = enemy_left
                self.enemy_party.members[0].damage_position = damage_left
                self.enemy_party.members[1].position = enemy_right
                self.enemy_party.members[1].damage_position = damage_right

            if len(self.enemy_party.members) == 3:
                self.enemy_party.members[0].position = enemy_far_left
                self.enemy_party.members[0].damage_position = damage_far_left
                self.enemy_party.members[1].position = enemy_center
                self.enemy_party.members[1].damage_position = damage_center
                self.enemy_party.members[2].position = enemy_far_right
                self.enemy_party.members[2].damage_position = damage_far_right

        @property
        def active_fighters(self):
            return [fighter for fighter in self.fighters if not fighter.ko]

        @property
        def someone_has_priority(self):
            return any(fighter.priority >= MAX_PRIORITY for fighter in self.active_fighters)

        @property
        def player_party_defeat(self):
            return all([player.ko for player in self.player_party.members])

        @property
        def enemy_party_defeat(self):
            return all([enemy.ko for enemy in self.enemy_party.members])

        @property
        def active_enemies(self):
            return [enemy for enemy in self.enemy_party.members if not enemy.ko]

        def fire_event(self, event_name='', data=None):
            e = Event(event_name, None)
            self.run_event(e)

        def announce(self, text, speaker=announcer):
            renpy.say(announcer, text + "{fast}{w=3}{nw}")

        def display_outcome(self, effect_display=None, effect_type=None, target=None, crit=False):
            outcome_display = "{}".format(effect_display)

            if crit:
                outcome_display = "{}!!".format(effect_display)

            colour = '#fff'
            outline_colour = '#000'

            if effect_type == 'healing':
                colour = '#6fa'

            outcome_text_style = {
                'color': colour,
                'outlines': [(2, outline_colour)],
                'size': 40,
            }

            outcome_text = Text(outcome_display, **outcome_text_style)

            renpy.show_screen('outcome_display', outcome_text=outcome_text, damage_position=target.damage_position, character_type=target.character_type)

        def run_event(self, e):
            for i, battle_event in enumerate(self.battle_events):

                # check which events match the trigger
                for trigger in battle_event.event_triggers:

                    # check if condition is met
                    if e.name == trigger:

                        # get list of speaker names
                        speakers = [speaker.name for (speaker, dialogue) in battle_event.event_text]

                        # get fighters who match
                        fighters = [fighter for fighter in self.fighters if fighter.name in speakers]

                        # can't speak if you're KO'd
                        if all([not fighter.ko for fighter in fighters]):

                            for (fighter, dialogue) in battle_event.event_text:
                                self.announce("{b}" + fighter.name + ":{/b} " + dialogue)

                            # remove the event
                            self.battle_events.pop(i)

        def run_turn(self, active_fighter):
            active_fighter.active = True

            active_fighter.update_status_effects()

            # Allows the player to cancel out of target selection
            # if they picked the wrong skill
            # not None because renpy disables choices whose values are None
            target = False

            while target is False:
                (skill, target) = active_fighter.get_action(enemies=self.active_enemies, players=self.player_party.members)

            effect = active_fighter.use_skill(skill, target)

            # don't bother announcing generic skills like Attack and Heal
            if skill.special:
                self.announce('{}'.format(skill.name))

            renpy.with_statement(skill.effect)

            effect_display, effect_type = target.resolve_effect(effect)

            self.display_outcome(effect_display=effect_display, effect_type=effect_type, target=target, crit=effect.crit)

            # check for a KO
            if target.ko:
                self.fire_event(target.name + ' defeated')

                if target.character_type == 'player':
                    target.knock_out()

            # set priority back to zero
            active_fighter.priority = 0
            active_fighter.active = False

        def run(self):
            renpy.suspend_rollback(True)
            self.running = True
            self.assign_priorities()

            renpy.show_screen('battle', battle=self)

            self.fire_event(event_name='battle start')

            while self.running:
                # figure out priority
                while not self.someone_has_priority:
                    self.tick()

                # run turn for whoever has priority
                top_priority = [fighter for fighter in self.active_fighters if fighter.priority >= MAX_PRIORITY]

                # take the first fighter to avoid ties
                active_fighter = top_priority[0]

                if active_fighter.ko:
                    continue

                self.run_turn(active_fighter)

                renpy.pause(0.5, hard=True)

                if self.player_party_defeat or self.enemy_party_defeat:
                    self.running = False

                    if self.player_party_defeat:
                        self.fire_event(event_name='player party defeated')
                        if self.player_defeated_message:
                            self.announce(self.player_defeated_message)
                        self.success = False
                        break

                    elif self.enemy_party_defeat:
                        self.fire_event(event_name='enemy party defeated')
                        if self.enemy_defeated_message:
                            self.announce(self.enemy_defeated_message)
                        self.success = True
                        break

            self.end()
            return self.success

        def end(self):
            self.allocate_exp()
            renpy.hide_screen(tag='battle')
            renpy.suspend_rollback(False)

        def allocate_exp(self):

            exp_earned = sum([enemy.exp_reward for enemy in self.enemy_party.members])

            eligible_players = [player for player in self.player_party.members if not player.ko]

            if eligible_players:
                self.announce("Earned {}EXP!".format(exp_earned))

            for player in eligible_players:

                player.add_exp(exp_earned)

                if player.ready_to_level_up:
                    player.apply_pending_exp()
                    self.announce("Level up!")
                    self.announce("{} reached level {}!".format(player.name, player.level))

        def assign_priorities(self):
            for fighter in self.fighters:
                fighter.priority = random.randint(0, MAX_PRIORITY)

        def tick(self):
            renpy.pause(0.01, hard=True)
            for fighter in self.fighters:
                fighter.tick()
