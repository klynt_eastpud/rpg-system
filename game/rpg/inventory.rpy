
init -3 python:


    class PlayerInventory(object):
        def __init__(self, player_party):
            self.items = {
                'weapon': [],
                'armour': [],
                'accessory': [],
                'consumable': [],
            }
            self.player_party = player_party

        @property
        def weapons(self):
            return self.items['weapon']

        @property
        def armour(self):
            return self.items['armour']

        @property
        def accessories(self):
            return self.items['accessory']

        @property
        def consumables(self):
            return self.items['consumable']

        @property
        def equippable_items_display(self):
            return [
                ('Weapons', self.weapons),
                ('Armour', self.armour),
                ('Accessories', self.accessories),
            ]

        @property
        def consumable_items_display(self):
            return [
                ('Consumables', self.consumables)
            ]

        def add_items(self, items):
            for item in items:
                self.items[item.item_type].append(item)

        def remove_item(self, item):
            i = self.items[item.item_type].index(item)
            del self.items[item.item_type][i]


    class GenericItem(object):
        def __init__(self, name, description):
            self.name = name
            self.description = description
            # generate unique ID for this item
            self.uuid = uuid.uuid1()


    class ConsumableItem(GenericItem):
        equippable = False
        item_type = 'consumable'

        def __init__(self, target=None, affected_stats=['current_hp'], amount=0, *args, **kwargs):
            super(ConsumableItem, self).__init__(*args, **kwargs)
            self.affected_stats = affected_stats
            self.amount = amount
            self.target = target

        @property
        def stat_description(self):
            # currently only HP restoration
            return '{} {}{}'.format(
                STAT_NAME_MAPPING[self.affected_stats[0]],
                '+' if self.amount >= 0 else '',
                self.amount
            )

        @property
        def full_description(self):
            return [self.description, self.stat_description]

        @property
        def effect(self):
            return {
                self.affected_stats[0]: self.amount
            }

        @property
        def tooltip(self):
            return {
                'equippable': self.equippable,
                'description': self.full_description,
                'effect': self.effect,
                'affected_stats': self.affected_stats,
                'target': self.target,
            }

    class StatModifier(object):
        """
            See `StatModifierStatus` for temporary status effects.
        """
        def __init__(self, amount=0, affected_stat=None):
            self.affected_stat = affected_stat
            # number increase/decrease
            self.amount = amount
            # generate unique ID for this status effect
            self.uuid = uuid.uuid1()


    class EquippableItem(GenericItem):
        equippable = True

        def __init__(self, stat_modifiers, target=None, *args, **kwargs):
            super(EquippableItem, self).__init__(*args, **kwargs)
            self.stat_modifiers = stat_modifiers
            self.target = target

        @property
        def stat_description(self):
            description = []
            if self.stat_modifiers:
                description = [
                    '{} {}{}'.format(
                        STAT_NAME_MAPPING[modifier.affected_stat],
                        '+' if modifier.amount >= 0 else '',
                        modifier.amount
                    )
                    for modifier in self.stat_modifiers
                ]
            return '\n'.join(description)

        @property
        def full_description(self):
            return [self.description, self.stat_description]

        @property
        def effect(self):
            return {
                modifier.affected_stat: modifier.amount
                for modifier in self.stat_modifiers
            }

        @property
        def unequip_effect(self):
            return {
                modifier.affected_stat: 0 - modifier.amount
                for modifier in self.stat_modifiers
            }

        @property
        def affected_stats(self):
            return [modifier.affected_stat for modifier in self.stat_modifiers]

        @property
        def equip_tooltip(self):
            return {
                'equippable': self.equippable,
                'description': self.full_description,
                'effect': self.effect,
                'affected_stats': self.affected_stats,
                'target': self.target,
            }

        @property
        def unequip_tooltip(self):
            return {
                'equippable': self.equippable,
                'description': self.full_description,
                'effect': self.unequip_effect,
                'affected_stats': self.affected_stats,
                'target': self.target,
            }


    class Item(ConsumableItem):
        pass


    class Weapon(EquippableItem):
        item_type = 'weapon'

    class Armour(EquippableItem):
        item_type = 'armour'


    class Accessory(EquippableItem):
        item_type = 'accessory'
