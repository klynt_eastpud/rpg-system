
init offset = -2
init:
    # Characters
    define Yxil = Character("Yxil")
    define Jaro = Character("Jaro")
    define Max = Character("Scarlet-clad pirate")
    define Vela = Character("Baton-wielding pirate")
    define Xephyr = Character("Visor-wearing pirate")

    # turn this on in the developer console while the game is running
    define debug_mode = False

    # Backgrounds
    image forest = "images/forest.png"

    ##-BACKGROUNDS-##

    ## COLOURS
    image white = Solid("#fff")
    image black = Solid("#000")
    image white50 = Color("#fff", alpha=0.5)
    image black50 = Color("#000", alpha=0.5)
    image red = Solid("#801005")

    # Transforms/transitions
    define dissolve = Dissolve(0.25)
    define long_dissolve = Dissolve(0.5)
    define very_long_dissolve = Dissolve(2.0)
    define fade = Fade(0.5, 0.25, 0.5)
    define long_fade = Fade(1, 0.5, 1)
    define very_long_fade = Fade(2, 0.5, 2)

# The game starts here.

label start:

    scene forest

    window hide

    python:

        show_quick_menu = False

        jaro = PlayerCharacter(
            name='Jaro',
            base_stats=BaseStats(vitality=7, defence=4, strength=3, speed=3),
            level=1,
        )

        yxil = PlayerCharacter(
            name='Yxil',
            base_stats=BaseStats(vitality=3, defence=5, strength=2, speed=5),
            registered_skills=[Overclock, Fortitude, Courage],
            level=1,
        )

        player_party = Party(character_type='player', members=[jaro, yxil])

        xephyr = AndroidModelXephyr(level=2)
        vela = VelaOrganon(level=2)
        maksutov = LeonMaksutov(level=2)

        pirate_party = Party(
            character_type='enemy',
            members=[
                xephyr, vela, maksutov
            ]
        )

        battle_events = [
            BattleEvent(
                event_text=[
                    (Jaro, "You can make characters talk during a battle with battle events."),
                    (Jaro, "Battle events currently include:"),
                    (Jaro, "'battle start' which is fired immediately..."),
                    (Jaro, "...and '(Character name) defeated' which is fired when a character is KO'd"),
                ],
                event_triggers=["battle start"]
            ),
            BattleEvent(
                event_text=[
                    (Jaro, "Yxil no!")
                ],
                event_triggers=["Yxil defeated"]
            ),
            BattleEvent(
                event_text=[
                    (Yxil, "Jaro!!")
                ],
                event_triggers=["Jaro defeated"]
            ),
        ]

        BattleInstance(
            enemy_party=pirate_party,
            player_party=player_party,
            battle_events=battle_events,
        ).run()

        show_quick_menu = True

        # restore everyone's hp
        yxil.restore_hp_to_full()
        jaro.restore_hp_to_full()

    python:

        inventory = PlayerInventory(player_party=player_party)

        botanical_drink = Item(
            name='Botanical drink',
            description='So refreshing...',
            affected_stats=['current_hp'],
            amount=100,
        )

        combat_knife = Weapon(
            name='Combat knife',
            description="Military-grade weapon for hand-to-hand combat.",
            stat_modifiers=[
                StatModifier(amount=2, affected_stat='strength'),
            ]
        )

        plasma_rifle = Weapon(
            name='Plasma rifle',
            description="A large, heavy energy weapon. Hard to handle but powerful.",
            stat_modifiers=[
                StatModifier(amount=5, affected_stat='strength'),
                StatModifier(amount=-2, affected_stat='speed'),
            ]
        )

        leather_jacket = Armour(
            name='Leather jacket',
            description="If you wear this you'll look 15% cooler.",
            stat_modifiers=[
                StatModifier(amount=2, affected_stat='defence'),
                StatModifier(amount=100, affected_stat='max_hp'),
            ]
        )

        bulletproof_vest = Armour(
            name='Bulletproof vest',
            description="Extremely durable chest protection.",
            stat_modifiers=[
                StatModifier(amount=5, affected_stat='defence'),
                StatModifier(amount=100, affected_stat='max_hp'),
            ]
        )

        titanium_ring = Accessory(
            name='Titanium ring',
            description="This is totally a piece of jewellery and not just a discarded piece of machinery.",
            stat_modifiers=[
                StatModifier(amount=2, affected_stat='speed'),
            ]
        )

        smart_glasses = Accessory(
            name='Smart glasses',
            description="Rumours of these selling your personal data in order to serve you ads are completely unfounded.",
            stat_modifiers=[
                StatModifier(amount=3, affected_stat='speed'),
            ]
        )

        inventory.add_items([
            botanical_drink,
            combat_knife,
            plasma_rifle,
            leather_jacket,
            bulletproof_vest,
            titanium_ring,
            smart_glasses,
        ])

        yxil.equip_item(plasma_rifle)
        yxil.equip_item(bulletproof_vest)
        yxil.equip_item(titanium_ring)
        jaro.equip_item(combat_knife)
        jaro.equip_item(leather_jacket)
        jaro.equip_item(smart_glasses)


    "Inventory"
    "Click the inventory button to open the inventory"

    python:
        show_quick_menu = False

        for pirate in pirate_party.members:
            pirate.restore_hp_to_full()

        pirate_party.members.remove(maksutov)
        yxil.attack = True
        yxil.registered_skills.append(Heal)

        vela.level = 2
        xephyr.level = 2

        outcome = BattleInstance(
            enemy_party=pirate_party,
            player_party=player_party,
            battle_events=[],
        ).run()

        show_quick_menu = True

        # restore everyone's hp
        yxil.restore_hp_to_full()
        jaro.restore_hp_to_full()

    "Different enemy skills can be weighted according to how often you want them to use them"

    python:
        player_party.members = [jaro]

        jaro.restore_hp_to_full()
        jaro.registered_skills = [Repair]

        time_entity = TimeEntity(level=3)

        enemy_party = Party(
            character_type='enemy',
            members=[time_entity]
        )

        BattleInstance(
            enemy_party=enemy_party,
            player_party=player_party,
            battle_events=battle_events,
            player_defeated_message="Defeat.",
            enemy_defeated_message=None,
        ).run()

    return
